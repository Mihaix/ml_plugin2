<?php 

/*
Plugin Name: Metadatos de video para mis posts
Plugin URL: 
Description: Segundo plugin
Version: 1.0
Author: Mihai
Author URI:
Licence: Free
*/ 

//FILTROS
//http://codex.wordpress.org/Plugin_API/Filter_Reference/


//Mostramos un label
add_action('add_meta_boxes', 'ml_add_metabox');

//Guardamos en la base de datos
add_action('save_post', 'ml_save_metabox');

//Registrar Widget
add_action('widgets_init', 'ml_widget_init');



function ml_add_metabox() {
    //doc http://codex.wordpress.org/Function_Reference/add_meta_box
    add_meta_box('ml_youtube', 'YouTube Video Link','ml_youtube_handler', 'post');
}

function ml_youtube_handler() {
    $value = get_post_custom($post->ID);
    $youtube_link = esc_attr($value['ml_youtube'][0]);
    echo '<label for="ml_youtube">YouTube Video Link </label><input type="text" id="ml_youtube" name="ml_youtube" value="'.$youtube_link.'" /><br>';
    echo '<label for="ml_youtube2">Name Video </label><input type="text" id="ml_youtube2" name="ml_youtube2" value="'.$youtube_link2.'" />';
}

 /* 
 Save Metadata
 */
function ml_save_metabox($post_id) {
    //don't save metadata if it's autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return; 
    }    
    
    //check if user can edit post
    if( !current_user_can( 'edit_post' ) ) {
        return;  
    }
    
    if( isset($_POST['ml_youtube'] )) {
        update_post_meta($post_id, 'ml_youtube', $_POST['ml_youtube']);
    }
    if( isset($_POST['ml_youtube2'] )) {
        update_post_meta($post_id, 'ml_youtube2', $_POST['ml_youtube2']);
    }
}

/**
 * register widget -> Funcion para registrar el Widget (llamada por add_action en la parte superior de nuestro plugin)
 */

function ml_widget_init() {
    register_widget(Ml_Widget);
}


//Tenemos que crear la nueva clase
/**
 * widget class
 */
class Ml_Widget extends WP_Widget {
    function Ml_Widget() {
        $widget_options = array(
            'classname' => 'ml_class', //CSS
            'description' => 'Show a YouTube Video from post metadata'
        );
        
        $this->WP_Widget('ml_id', 'YouTube Video', $widget_options);
    }
    
    /**
     * show widget form in Appearence / Widgets
     */
    function form($instance) {
        $defaults = array('title' => 'Video');
        $instance = wp_parse_args( (array) $instance, $defaults);
        
        $title = esc_attr($instance['title']);
        
        echo '<p>Title <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
    }
    
    /**
     * save widget form
     */
    function update($new_instance, $old_instance) {
        
        $instance = $old_instance;        
        $instance['title'] = strip_tags($new_instance['title']);        
        return $instance;
    }
    
    /**
     * show widget in post / page
     */
    function widget($args, $instance) {
        extract( $args );        
        $title = apply_filters('widget_title', $instance['title']);
        
        //show only if single post
        if(is_single()) {
            echo $before_widget;
            echo $before_title.$title.$after_title;
            
            //get post metadata
            $ml_youtube = get_post_meta(get_the_ID(), 'ml_youtube', true);

            $ml_youtube2 = get_post_meta(get_the_ID(), 'ml_youtube2', true);
            
            //print widget content
            echo '<iframe width="400" height="200" frameborder="0" allowfullscreen src="http://www.youtube.com/embed/'.($ml_youtube).'"></iframe>';       
            
            echo $ml_youtube2;
            
            echo $after_widget;
        }
    }
}
?>